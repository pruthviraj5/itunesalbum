//
//  ForgotPasswordViewController.swift
//  iTunesAlbum
//
//  Created by Pruthviraj Middela on 7/3/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailOrPhoneNumberTextField: UITextField!
    @IBOutlet weak var resetPasswordButton: UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
